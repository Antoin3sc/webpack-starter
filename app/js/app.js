require('../scss/app.scss');

//Include fonts
const fontCtx = require.context('../fonts', false, /\.(otf)$/);
fontCtx.keys().forEach(fontCtx);

// Include Images
const imagesCtx = require.context('../images', true, /\.(png|jpg|jpeg|gif|ico|svg|webp)$/);
imagesCtx.keys().forEach(imagesCtx);